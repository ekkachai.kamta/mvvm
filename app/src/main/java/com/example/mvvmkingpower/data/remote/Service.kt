package com.example.mvvmkingpower.data.remote

import com.example.mvvmkingpower.data.model.Photo
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Path

interface Service {
    @GET("albums/{id}/photos")
    fun fetchPhotos(
        @Path("id") id: Int
    ): Flowable<List<Photo>>
}