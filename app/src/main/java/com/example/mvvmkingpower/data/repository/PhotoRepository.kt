package com.example.mvvmkingpower.data.repository

import com.example.mvvmkingpower.data.model.Photo
import io.reactivex.Flowable

interface PhotoRepository {
    fun fetchPhotos(page: Int): Flowable<List<Photo>>
}