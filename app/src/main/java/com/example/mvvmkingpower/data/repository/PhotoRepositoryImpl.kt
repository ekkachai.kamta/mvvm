package com.example.mvvmkingpower.data.repository

import com.example.mvvmkingpower.data.model.Photo
import com.example.mvvmkingpower.data.remote.Service
import io.reactivex.Flowable

class PhotoRepositoryImpl(
    private val remote: Service
) : PhotoRepository {
    override fun fetchPhotos(page: Int): Flowable<List<Photo>> {
       return remote.fetchPhotos(page)
    }
}