package com.example.mvvmkingpower.util.ext

import retrofit2.HttpException
import java.net.UnknownHostException

fun Throwable.handleErrorBody(): Exception {
    return when (this) {
        is HttpException -> {
            this.response()?.errorBody()?.let {
                Exception(message.toString())

            } ?: run {
                Exception(this)
            }
        }
        else -> Exception(this)
    }
}