package com.example.mvvmkingpower.util.ext

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

inline fun FragmentActivity.replaceFragment(containerViewId: Int, f:() -> Fragment): Fragment? {
    return f().apply { supportFragmentManager.beginTransaction().replace(containerViewId, this).commit() }
}

inline fun FragmentActivity.replaceAddFragment(containerViewId: Int, f:() -> Fragment): Fragment?{
    return f().apply { supportFragmentManager.beginTransaction().replace(containerViewId,this).addToBackStack(null).commit() }
}

