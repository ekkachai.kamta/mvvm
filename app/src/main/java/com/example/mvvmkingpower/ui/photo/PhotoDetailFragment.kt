package com.example.mvvmkingpower.ui.photo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.mvvmkingpower.R
import com.example.mvvmkingpower.data.model.Photo
import com.example.mvvmkingpower.databinding.FragmentPhotoDetailBindingImpl
import com.example.mvvmkingpower.ui.base.BaseFragment
import com.example.mvvmkingpower.util.ext.replaceAddFragment
import com.example.mvvmkingpower.util.ext.toVisible
import kotlinx.android.synthetic.main.fragment_photo_detail.*
import kotlinx.android.synthetic.main.view_toolbar.*
import timber.log.Timber

class PhotoDetailFragment : BaseFragment() {
    private lateinit var photo: Photo
    private lateinit var binding: FragmentPhotoDetailBindingImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
           photo = it.getParcelable<Photo>(PHOTO) as Photo
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_photo_detail,container,false)
        binding?.apply {
            lifecycleOwner = this@PhotoDetailFragment
            item = photo

        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

    }

    private fun initView() {
        title.text = photo.title
        backPage()
    }

    private fun backPage() {
        back.toVisible()
        back.setOnClickListener { activity?.apply { onBackPressed() } }
    }

    companion object {
        private const val PHOTO = "photo"
        @JvmStatic
        fun newInstance(photo: Photo) =
            PhotoDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(PHOTO, photo)
                }
            }
    }
}