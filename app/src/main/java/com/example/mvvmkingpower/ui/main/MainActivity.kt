package com.example.mvvmkingpower.ui.main

import android.os.Bundle
import android.os.PersistableBundle
import androidx.fragment.app.FragmentActivity
import com.example.mvvmkingpower.R
import com.example.mvvmkingpower.ui.base.BaseActivity
import com.example.mvvmkingpower.util.ext.replaceFragment

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(savedInstanceState != null){
            val fragment =  supportFragmentManager.getFragment(savedInstanceState, CURRENT_FRAGMENT)
            fragment?.let {
                replaceFragment(R.id.containerFragment){ it }
            }

        }else{
            replaceFragment(R.id.containerFragment, ::MainFragment)

        }
    }


    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        val current = supportFragmentManager.findFragmentById(R.id.containerFragment)
        current?.let { supportFragmentManager.putFragment(outState, CURRENT_FRAGMENT,it) }
    }

    companion object{
        const val CURRENT_FRAGMENT = "current_fragment"
    }
}