package com.example.mvvmkingpower.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvmkingpower.data.model.Photo
import com.example.mvvmkingpower.data.remote.result.Response
import com.example.mvvmkingpower.data.repository.PhotoRepositoryImpl
import com.example.mvvmkingpower.ui.base.BaseViewModel
import com.example.mvvmkingpower.util.ext.handleErrorBody
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel(
    private val repositoryImpl: PhotoRepositoryImpl
) : BaseViewModel() {

    private val _photos = MutableLiveData<Response<List<Photo>>>()
    val photos: LiveData<Response<List<Photo>>>
        get() = _photos

    fun fetchPhotos(){
        val disposable = repositoryImpl.fetchPhotos(1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.isEmpty()){
                    _photos.postValue(Response.Empty)
                }else{
                    _photos.postValue(Response.Success(it))
                }
            },{
                _photos.postValue(Response.Error(it.handleErrorBody()))

            })
        addDisposable(disposable)
    }
}