package com.example.mvvmkingpower.ui.main.adapter

import android.R
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmkingpower.data.model.Photo
import com.example.mvvmkingpower.databinding.ItemViewPhotoBinding
import kotlinx.android.synthetic.main.item_view_photo.view.*


class PhotosAdapter(
    private val eventPhoto: EventPhoto
): RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder>() {

    var items: List<Photo> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemViewPhotoBinding = ItemViewPhotoBinding.inflate(inflater, parent, false)
        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {

        holder.binding.apply {
            item = items[position]

            imageView.setOnClickListener {
                eventPhoto.showDetail(items[position])

            }
        }
    }

    override fun getItemCount(): Int = items.size

    inner class PhotoViewHolder(val binding: ItemViewPhotoBinding): RecyclerView.ViewHolder(binding.root)
}

interface EventPhoto{
    fun showDetail(item: Photo)
}