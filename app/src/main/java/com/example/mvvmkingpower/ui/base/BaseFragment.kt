package com.example.mvvmkingpower.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.mvvmkingpower.R
import kotlinx.android.synthetic.main.view_toolbar.*

abstract class BaseFragment : Fragment() {


    private fun updateToolbar() {
       val activity = context as AppCompatActivity
        activity?.apply {
          setSupportActionBar(findViewById(R.id.toolbar))
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
        updateToolbar()
    }
}