package com.example.mvvmkingpower.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvmkingpower.R
import com.example.mvvmkingpower.data.model.Photo
import com.example.mvvmkingpower.data.remote.result.Response
import com.example.mvvmkingpower.databinding.FragmentMainBinding
import com.example.mvvmkingpower.ui.base.BaseFragment
import com.example.mvvmkingpower.ui.main.adapter.EventPhoto
import com.example.mvvmkingpower.ui.main.adapter.PhotosAdapter
import com.example.mvvmkingpower.ui.photo.PhotoDetailFragment
import com.example.mvvmkingpower.util.ext.replaceAddFragment
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

class MainFragment : BaseFragment(), EventPhoto {
    private lateinit var binding: FragmentMainBinding
    private lateinit var photosAdapter: PhotosAdapter
    private val viewModel: MainViewModel by sharedViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.i("onCreate")
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container,false)
        binding.apply {
            lifecycleOwner = this@MainFragment
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        photosAdapter = PhotosAdapter(this)
        photosObserve()
        recycleView()

    }

    private fun recycleView() {
        binding.photosRecycle.apply {
            layoutManager = GridLayoutManager(context,2)
            adapter = photosAdapter

        }
    }

    private fun photosObserve() {
        viewModel.fetchPhotos()
        viewModel.photos.observe(this, Observer {
            when(it){
                is Response.Success -> {
                    photosAdapter.apply {
                        items = it.data
                        notifyDataSetChanged()
                    }
                }
                is Response.Error -> {
                    Toast.makeText(requireContext(),it.toString(), Toast.LENGTH_LONG).show()
                }
                else -> {}
            }
        })

    }

    override fun showDetail(item: Photo) {
        activity?.apply { replaceAddFragment(R.id.containerFragment){
            PhotoDetailFragment.newInstance(item)

        } }
    }


}