package com.example.mvvmkingpower.di

import com.example.mvvmkingpower.data.remote.Remote
import com.example.mvvmkingpower.data.remote.Service
import com.example.mvvmkingpower.data.repository.PhotoRepositoryImpl
import com.example.mvvmkingpower.ui.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val appModule = module {
    viewModel { MainViewModel(get()) }
    factory { PhotoRepositoryImpl(get()) }
}

val remote : Retrofit = Remote.builder()
val service: Service = remote.create(Service::class.java)
val networkModule = module {
    single { service }
}
