package com.example.mvvmkingpower

import android.app.Application
import androidx.multidex.MultiDex
import com.example.mvvmkingpower.di.appModule
import com.example.mvvmkingpower.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class App: Application() {
    init {

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(arrayListOf(networkModule,appModule))
        }

        

        MultiDex.install(this)
    }
}